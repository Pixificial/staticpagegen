/*
 * Copyright (c) 2023-2024 Kaan Çırağ
 *
 * This file is part of staticpagegen.
 *
 * staticpagegen is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License only.
 * 
 * staticpagegen is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with staticpagegen. If not, see <https://www.gnu.org/licenses/>.
 */
#include "xhtmlbasic1-0.h"

char xhtmlbasic10top[] = "<?xml version=\"1.0\"?>\n\
<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML Basic 1.0//EN\"\n\
    \"http://www.w3.org/TR/xhtml-basic/xhtml-basic10.dtd\">\n\
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\">\n\
  <head>\n\
    <title>";
char xhtmlbasic10mid1[] = "</title>\n\
  </head>\n\
  <body>\n\
    <h1><a href=\"/\">";
char xhtmlbasic10mid2[] = "</a></h1>\n\
    <pre>\n";
char xhtmlbasic10bot[] = "\
    </pre>\n\
  </body>\n\
</html>\n";
